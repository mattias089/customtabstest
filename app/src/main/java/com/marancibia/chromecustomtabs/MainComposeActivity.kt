package com.marancibia.chromecustomtabs

import android.annotation.SuppressLint
import android.app.Activity
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.browser.customtabs.CustomTabsIntent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.marancibia.chromecustomtabs.ui.theme.ChromeCustomTabsTheme
import com.marancibia.chromecustomtabs.ui.theme.CustomColors
import com.marancibia.chromecustomtabs.ui.theme.CustomShapes
import com.marancibia.chromecustomtabs.ui.theme.White

class MainComposeActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            ChromeCustomTabsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Data()
                }
            }
        }
    }
}

@Composable
@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
fun Data() {
    val activity = (LocalContext.current as? Activity)

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = stringResource(id = R.string.app_name))
                },
                navigationIcon = {
                    IconButton(onClick = { activity?.finish() }) {
                        Icon(Icons.Filled.Close, "closeIcon")
                    }
                },
                backgroundColor = MaterialTheme.colors.primary,
                contentColor = White,
                elevation = 4.dp
            )
        },
        content = { Content() }
    )
}

@Composable
fun Content() {
    val context = LocalContext.current
    val buttonTextIds = listOf(
        R.string.button_tel,
        R.string.button_mailto,
        R.string.button_pdf,
        R.string.button_pdf2,
        R.string.button_terms_and_conditions,
    )

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier.wrapContentSize(),
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            buttonTextIds.forEach { textID ->
                ActionButton(textID = textID) {
                    var url: String? = null
                    val useViewer = true
                    val viewer = "https://docs.google.com/viewer?url="

                    when (textID) {
                        R.string.button_tel -> {
                            url = "tel:+56967264181"
                        }

                        R.string.button_mailto -> {
                            url = "mailto:matias.arancibia.89@gmail.com"
                        }

                        R.string.button_pdf -> {
                            url =
                                "https://assets.ctfassets.net/ynglr8ha4zwh/7qahuVASc2kqVRKZ396TEH/51dea5a6bd6b47a492689857f75c103b/CMR_PUNTOS___Catalogo_TOTTUS_FOOD_MARZO.pdf"

                            if (useViewer) {
                                url = viewer.plus(url)
                            }
                        }

                        R.string.button_pdf2 -> {
                            url =
                                "https://resource.csnstatic.com/redbook/smartbuyerinsights/2023/3/2/aba4cc77-63b3-4a57-83d7-a128caa14b7d.ppsr.pdf"

                            if (useViewer) {
                                url = viewer.plus(url)
                            }
                        }

                        R.string.button_terms_and_conditions -> {
                            url = "https://www.carsales.com.au/account/fuel/terms-and-conditions"
                        }
                    }

                    CustomTabsIntent.Builder()
                        .build()
                        .launchUrl(context, Uri.parse(url))
                }
            }
        }
    }
}

@Composable
private fun ActionButton(
    textID: Int,
    onClickAction: () -> Unit
) {
    OutlinedButton(
        modifier = Modifier.width(280.dp),
        border = CustomColors.customBorderStrokeColors(),
        elevation = ButtonDefaults.elevation(
            defaultElevation = 5.dp,
            hoveredElevation = 6.dp,
            pressedElevation = 1.dp,
        ),
        shape = CustomShapes.customShapes(),
        colors = CustomColors.customButtonColors(),
        contentPadding = PaddingValues(all = 18.dp),
        onClick = { onClickAction.invoke() }
    ) {
        Text(
            text = stringResource(id = textID),
            color = CustomColors.customTextColors(),
            fontSize = 17.sp,
            fontWeight = FontWeight.Bold
        )
    }
}

@Preview(showBackground = true)
@Composable
fun DataPreview() {
    ChromeCustomTabsTheme {
        Data()
    }
}