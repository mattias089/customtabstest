package com.marancibia.chromecustomtabs.ui.theme

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.ButtonColors
import androidx.compose.material.ButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

/**
 * Created by Matias Arancibia on 03 may., 2023
 * Copyright (c) 2023 Carsales. All rights reserved.
 */

val White = Color(0xFFFFFFFF)
val Black = Color(0xFF000000)
val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val Teal700 = Color(0xFF018786)
val Cyan400 = Color(0xFF26C6DA)
val Cyan500 = Color(0xFF00BCD4)
val Blue200 = Color(0xFF90CAF9)
val Blue500 = Color(0xFF2196F3)
val Blue700 = Color(0xFF1976D2)
val LightBlue200 = Color(0xFF81D4FA)
val LightBlue400 = Color(0xFF29B6F6)
val LightBlue500 = Color(0xFF03A9F4)
val LightBlue700 = Color(0xFF1976D2)
val LightBlue800 = Color(0xFF1565C0)

sealed class CustomColors {

    companion object {

        @Composable
        fun customBorderStrokeColors(): BorderStroke {
            return BorderStroke(
                width = 3.dp,
                color = if (isSystemInDarkTheme()) {
                    Cyan400
                } else {
                    LightBlue700
                }
            )
        }

        @Composable
        fun customTextColors(): Color {
            return if (isSystemInDarkTheme()) {
                Cyan400
            } else {
                LightBlue700
            }
        }

        @Composable
        fun customButtonColors(): ButtonColors {
            return ButtonDefaults.outlinedButtonColors(
                backgroundColor = if (isSystemInDarkTheme()) Black else White,
                contentColor = if (isSystemInDarkTheme()) White else LightBlue700,
            )
        }
    }
}