package com.marancibia.chromecustomtabs.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.shape.CornerBasedShape
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.dp

val DefaultRoundedShapes = Shapes(
    small = RoundedCornerShape(10.dp),
    medium = RoundedCornerShape(25.dp),
    large = RoundedCornerShape(50.dp)
)

val CustomRoundedCornerShape = RoundedCornerShape(
    topStart = 22.dp,
    topEnd = 8.dp,
    bottomEnd = 22.dp,
    bottomStart = 22.dp
)

val CustomCutCornerShape = CutCornerShape(
    topStart = 10.dp,
    topEnd = 10.dp,
    bottomEnd = 10.dp,
    bottomStart = 10.dp
)

sealed class CustomShapes {
    companion object {

        @Composable
        fun customShapes(): CornerBasedShape {
            return if (isSystemInDarkTheme()) {
                CustomRoundedCornerShape
            } else {
                CustomCutCornerShape
            }
        }
    }
}